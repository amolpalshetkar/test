#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    rowCount = 0;

    DataManager::createInstance("scca.db");

    updateList();

    connect(ui->tbStart,SIGNAL(clicked()),this,SLOT(startSlot()));
    connect(ui->tbReport,SIGNAL(clicked()),this,SLOT(repSlot()));
    connect(ui->tbAdd,SIGNAL(clicked()),this,SLOT(addItem()));
    connect(ui->tbRemove,SIGNAL(clicked()),this,SLOT(remItem()));

    ui->leQty->setText("1");

    sampleId = DataManager::getInstance()->getSampleId();
    if (sampleId == -1){
        sampleId = 1;
    }
    else{
        sampleId++;
    }
    ui->leId->setText(QString::number(sampleId));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startSlot(){
    qDebug() << "Start";
    QStringList list;
    list << "Item" << "Quantity" << "Pr";
    ui->twData->setRowCount(0);
    ui->twData->setColumnCount(3);
    ui->twData->setHorizontalHeaderLabels(list);
    rowCount=0;
    ui->tbTotal->setEnabled(true);

    sampleId = DataManager::getInstance()->getSampleId();
    if (sampleId == -1){
        sampleId = 1;
    }
    else{
        sampleId++;
    }
    ui->leId->setText(QString::number(sampleId));
    qDebug() << "sid:" << sampleId;

}

void MainWindow::repSlot(){
    qDebug() << "Report";
}

void MainWindow::updateList(){
    QSqlQueryModel qs;
    QSqlRecord r;
    QString sql;
    sql = QString("select * from item order by code");

    int count = DataManager::getInstance()->getData(sql,&qs);

    for (int i=0;i<count;i++){
        r = qs.record(i);
        ui->lvData->addItem(new QListWidgetItem(r.value("name").toString()));
    }
    ui->lvData->setCurrentRow(0);


}

void MainWindow::addItem(){
    qDebug() << "Add";
    ui->twData->setRowCount(++rowCount);
    ui->twData->setColumnCount(3);
    ui->twData->setItem(rowCount-1,0,new QTableWidgetItem(QString(ui->lvData->currentItem()->text())));
    ui->twData->setItem(rowCount-1,1,new QTableWidgetItem(QString(ui->leQty->text())));
    ui->twData->setItem(rowCount-1,2,new QTableWidgetItem(QString(getPrice(ui->lvData->currentItem()->text()))));


}

void MainWindow::remItem(){
    qDebug() << "Remove";
    ui->twData->removeRow(rowSelected);
    rowCount = ui->twData->rowCount();
}

QString MainWindow::getPrice(QString str){
    QSqlQueryModel qs;
    QSqlRecord r;
    QString sql;
    sql = QString("select sp from item where name='%1'").arg(str);

    int count = DataManager::getInstance()->getData(sql,&qs);

    if (count>0){
        r = qs.record(0);
        //    qDebug() << "sp:" << r.value("sp").toString();
        double tot = r.value("sp").toInt() * ui->leQty->text().toDouble();
        qDebug() << "qty*rate:" << tot;

        return QString::number(tot,'f',2);
    }
    else{
        return "0";
    }


}

void MainWindow::on_tbTotal_clicked()
{
    ui->tbTotal->setEnabled(false);
    ui->twData->setRowCount(++rowCount);
    ui->twData->setItem(rowCount-1,1,new QTableWidgetItem("Total = "));
    double total=0;
    for (int i=0;i<rowCount-1;i++){
        total += ui->twData->item(i,2)->text().toDouble();
    }
    qDebug() << "Tot:" << total;
    ui->twData->setItem(rowCount-1,2,new QTableWidgetItem(QString::number(total,'f',2)));

}

void MainWindow::on_tbSave_clicked()
{
    unsigned long now = QDateTime::currentDateTime().toTime_t();
    QString sql;
    for(int i=0;i<rowCount-1;i++){
        sql = QString("insert into report(id,sid,custname,code,qty,sp) values('%1','%2','%3','%4','%5','%6')").arg(now+i).arg(sampleId).arg(ui->leName->text()).arg(getCode(ui->twData->item(i,0)->text())).arg(ui->twData->item(i,1)->text()).arg(ui->twData->item(i,2)->text());
        DataManager::getInstance()->execute(sql);
        qDebug() << "sql:" << sql;
//        sleep(1);
    }
}

void MainWindow::on_twData_cellClicked(int row, int column)
{
    rowSelected = row;
    qDebug() << "RS:" << rowSelected;
}

QString MainWindow::getCode(QString str){
    QSqlQueryModel qs;
    QSqlRecord r;
    QString sql;
    sql = QString("select code from item where name='%1'").arg(str);

    int count = DataManager::getInstance()->getData(sql,&qs);

    if (count>0){
        r = qs.record(0);
        int tot = r.value("code").toInt();
        //    qDebug() << "sp:" << r.value("sp").toString();
//        double tot = r.value("code").toInt() * ui->leQty->text().toDouble();
        qDebug() << "code:" << tot;

        return QString::number(tot);
    }
    else{
        return "0";
    }

}
