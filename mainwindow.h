#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQueryModel>
#include <QThread>
#include <QtGui>
#include <QDateTime>
#include "datamanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    int rowCount;
    int rowSelected;
    int sampleId;

    QSqlDatabase db;

    void updateList();
    QString getPrice(QString str);
    QString getCode(QString str);

private slots:
    void startSlot();
    void repSlot();
    void addItem();
    void remItem();

    void on_tbTotal_clicked();
    void on_tbSave_clicked();
    void on_twData_cellClicked(int row, int column);
};

#endif // MAINWINDOW_H
