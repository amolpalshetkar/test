/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Mar 11 09:50:58 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *tbStart;
    QPushButton *tbReport;
    QTableWidget *twData;
    QListWidget *lvData;
    QPushButton *tbAdd;
    QPushButton *tbRemove;
    QLineEdit *leQty;
    QPushButton *tbTotal;
    QPushButton *tbSave;
    QPushButton *tbPrint;
    QLineEdit *leName;
    QLabel *label;
    QLineEdit *leId;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(848, 432);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        tbStart = new QPushButton(centralWidget);
        tbStart->setObjectName(QString::fromUtf8("tbStart"));
        tbStart->setGeometry(QRect(10, 10, 91, 51));
        tbReport = new QPushButton(centralWidget);
        tbReport->setObjectName(QString::fromUtf8("tbReport"));
        tbReport->setGeometry(QRect(10, 70, 91, 51));
        twData = new QTableWidget(centralWidget);
        twData->setObjectName(QString::fromUtf8("twData"));
        twData->setGeometry(QRect(260, 40, 561, 361));
        lvData = new QListWidget(centralWidget);
        lvData->setObjectName(QString::fromUtf8("lvData"));
        lvData->setGeometry(QRect(110, 10, 91, 361));
        tbAdd = new QPushButton(centralWidget);
        tbAdd->setObjectName(QString::fromUtf8("tbAdd"));
        tbAdd->setGeometry(QRect(205, 80, 51, 41));
        tbRemove = new QPushButton(centralWidget);
        tbRemove->setObjectName(QString::fromUtf8("tbRemove"));
        tbRemove->setGeometry(QRect(206, 130, 51, 41));
        leQty = new QLineEdit(centralWidget);
        leQty->setObjectName(QString::fromUtf8("leQty"));
        leQty->setGeometry(QRect(205, 20, 51, 41));
        tbTotal = new QPushButton(centralWidget);
        tbTotal->setObjectName(QString::fromUtf8("tbTotal"));
        tbTotal->setGeometry(QRect(206, 200, 51, 41));
        tbSave = new QPushButton(centralWidget);
        tbSave->setObjectName(QString::fromUtf8("tbSave"));
        tbSave->setGeometry(QRect(206, 250, 51, 41));
        tbPrint = new QPushButton(centralWidget);
        tbPrint->setObjectName(QString::fromUtf8("tbPrint"));
        tbPrint->setGeometry(QRect(206, 300, 51, 41));
        leName = new QLineEdit(centralWidget);
        leName->setObjectName(QString::fromUtf8("leName"));
        leName->setGeometry(QRect(330, 10, 211, 21));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(290, 10, 61, 21));
        leId = new QLineEdit(centralWidget);
        leId->setObjectName(QString::fromUtf8("leId"));
        leId->setGeometry(QRect(620, 10, 71, 21));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(580, 10, 21, 21));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 848, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        tbStart->setText(QApplication::translate("MainWindow", "START", 0, QApplication::UnicodeUTF8));
        tbReport->setText(QApplication::translate("MainWindow", "REPORT", 0, QApplication::UnicodeUTF8));
        tbAdd->setText(QApplication::translate("MainWindow", "Add", 0, QApplication::UnicodeUTF8));
        tbRemove->setText(QApplication::translate("MainWindow", "Remove", 0, QApplication::UnicodeUTF8));
        tbTotal->setText(QApplication::translate("MainWindow", "Total", 0, QApplication::UnicodeUTF8));
        tbSave->setText(QApplication::translate("MainWindow", "Save", 0, QApplication::UnicodeUTF8));
        tbPrint->setText(QApplication::translate("MainWindow", "Print", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Name", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "ID", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
